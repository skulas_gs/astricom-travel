
function updateLocation() {
    //alert('boo');
	rememberPosition(document.getElementById('title').value, document.getElementById('description').value, 
                     sessionStorage.getItem('id'), null,
                     sessionStorage.getItem("currLat"),sessionStorage.getItem("currLong"),false);
	var db = GetDB();
    db.transaction(updateDB, errorCB, successUpdate); //save changed data to session storage and into the db
}

function updateDB(tx){
	var fields = ["title","description"];
	var values = [sessionStorage.getItem("title"),sessionStorage.getItem("description")];
	var connectionString = "id = '" + sessionStorage.getItem('id') + "'";
	var select = buildQuery("UPDATE", "LOCATIONS", fields, values, connectionString);	 
    console.log(select);
    tx.executeSql(select);
}

function successUpdate() {
    var db = GetDB();
    db.transaction(fetchDB); 
}

function deleteLocation(){
    var db = GetDB();
    db.transaction(delLoc, errorCB, successUpdate); 
}

function delLoc(tx){
    select = "DELETE FROM LOCATIONS WHERE ID = '" + sessionStorage.getItem('id') + "'";
    tx.executeSql(select);
}



function showPopup()
{
    $('<div>').simpledialog2({
                             mode: 'blank',
                             headerText: 'My Location',
                             headerClose: true,
                             callbackOpen:fillDetails,
                             blankContent : 
                             '<div data-role="content">' +
                             '<table style="vertical-align:middle; width:98%;">' +
                             '<tr><td align="center" valign="top"><input id="title" type="text" placeholder="Title" /></td></tr>' +
                             '<tr><td align="center" valign="top"  ><textarea id="description" class="message"maxlength="150" rows="10" cols="30" placeholder="Description"' +
                             'onchange="updateCountdown()" onkeyup="updateCountdown()"></textarea></td></tr>' +
                             '<tr><td align="right"><span class="countdown"></span></td></tr>' +
                             '<tr><td><button rel="close" class="small-button" data-inline="true" onclick="updateLocation();" >Save</button>' +
                             '<button rel="close" class="small-button" data-inline="true" onclick="deleteLocation();" >Delete</button></td></tr>' +
                             '<tr><td><button class="small-button" data-inline="true" onclick="directions();" >Directions</button>' +
                             '</td></tr></table></div>'                             
                             });
}

function fillDetails()
{
    document.getElementById("title").value = sessionStorage.getItem("title");
    document.getElementById("description").value = sessionStorage.getItem("description");
    updateCountdown(); //retreive title, description and set the char limit counter
    /*if ((sessionStorage.getItem("image") != "NOIMAGE") && (sessionStorage.getItem("image") != null))
    {
        var image = document.getElementById('largeImage');
        image.style.display = 'block';
        image.src = "data:image/jpeg;base64," + sessionStorage.getItem("image");
        alert(sessionStorage.getItem("image"));
        $('#imgBtn').hide();
    }
    else
        $('#imgBtn').show(); */        
}