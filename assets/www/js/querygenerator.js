function buildQuery(type, table, fields, values, conditionString){
	var query = type + " ";
	var field;
	var value;
	var i = fields.length - 1;
	
	switch (type)
	{
		case "INSERT":
		query += "INTO " + table + " (";
		for (field in fields)
			{
				query += fields[field];
				if (i > 0)
					query += ", ";
				i--;
			}
		query += ") VALUES (";
		i = values.length - 1;
		for (value in values)
		{
			query += "'" + values[value] + "'";
			if (i > 0)
				query += ", ";
			i--;
		}
		query += ")";
		break;
	case "UPDATE":
		j = 0;
		query += table + " SET ";
		for (field in fields)
			{
				query += fields[field] + " = '" + values[j] + "'";
				if (i > 0)
					query += ",";
				query += " ";
				i--;
				j++
			}
		query += "WHERE " + conditionString;
		break;
	}
	return query;
}