﻿
{"Airport": [
    {
    "AirportCode": "0M4",
    "FAACode": "0M4",
    "Name": "Benton Co"
  },
    {
    "AirportCode": "1K1",
    "FAACode": "1K1",
    "Name": "Benton"
  },
    {
    "AirportCode": "1N5",
    "FAACode": "1N5",
    "Name": "Bennett"
  },
    {
    "AirportCode": "3U7",
    "FAACode": "3U7",
    "Name": "Benchmark"
  },
    {
    "AirportCode": "42V",
    "FAACode": "42V",
    "Name": "Jones"
  },
    {
    "AirportCode": "49I",
    "FAACode": "49I",
    "Name": "Bennett"
  },
    {
    "AirportCode": "74N",
    "FAACode": "74N",
    "Name": "Bendigo"
  },
    {
    "AirportCode": "92A",
    "FAACode": "92A",
    "Name": "Chilhowee"
  },
    {
    "AirportCode": "BBB",
    "FAACode": "BBB",
    "IATACode": "BBB",
    "ICAOCode": "KBBB",
    "Name": "Benson Municipal Airport"
  },
    {
    "AirportCode": "BCW",
    "IATACode": "BCW",
    "Name": "Benguera Island Airport"
  },
    {
    "AirportCode": "BEB",
    "IATACode": "BEB",
    "ICAOCode": "EGPL",
    "Name": "Benbecula Airport"
  },
    {
    "AirportCode": "BEH",
    "FAACode": "BEH",
    "IATACode": "BEH",
    "ICAOCode": "KBEH",
    "Name": "Ross Field"
  },
    {
    "AirportCode": "BEN",
    "IATACode": "BEN",
    "ICAOCode": "HLLB",
    "Name": "Benina International Airport"
  },
    {
    "AirportCode": "BEX",
    "IATACode": "BEX",
    "ICAOCode": "EGUB",
    "Name": "Royal Air Force Station"
  },
    {
    "AirportCode": "BFU",
    "IATACode": "BFU",
    "Name": "Bengbu Airport"
  },
    {
    "AirportCode": "BGV",
    "IATACode": "BGV",
    "Name": "Bento Goncalves Airport"
  },
    {
    "AirportCode": "BJK",
    "IATACode": "BJK",
    "Name": "Benjina Airport"
  },
    {
    "AirportCode": "BJT",
    "IATACode": "BJT",
    "Name": "Bentota River Airport"
  },
    {
    "AirportCode": "BKS",
    "IATACode": "BKS",
    "ICAOCode": "WIPL",
    "Name": "Padangkemiling Airport"
  },
    {
    "AirportCode": "BLN",
    "IATACode": "BLN",
    "ICAOCode": "YBLA",
    "Name": "Benalla Airport"
  },
    {
    "AirportCode": "BLR",
    "IATACode": "BLR",
    "ICAOCode": "VOBL",
    "Name": "Bengaluru International Airport"
  },
    {
    "AirportCode": "BNC",
    "IATACode": "BNC",
    "ICAOCode": "FZNP",
    "Name": "Beni Airport"
  },
    {
    "AirportCode": "BNI",
    "IATACode": "BNI",
    "ICAOCode": "DNBE",
    "Name": "Benin City Airport"
  },
    {
    "AirportCode": "BSP",
    "IATACode": "BSP",
    "Name": "Bensbach Airport"
  },
    {
    "AirportCode": "BTN",
    "FAACode": "BBP",
    "IATACode": "BTN",
    "ICAOCode": "KBBP",
    "Name": "Bennettsville Airport"
  },
    {
    "AirportCode": "BUG",
    "IATACode": "BUG",
    "ICAOCode": "FNBG",
    "Name": "Gen V Deslandes Airport"
  },
    {
    "AirportCode": "BWY",
    "IATACode": "BWY",
    "Name": "Bentwaters Street Airport"
  },
    {
    "AirportCode": "BXG",
    "IATACode": "BXG",
    "Name": "Bendigo Airport"
  },
    {
    "AirportCode": "CKB",
    "FAACode": "CKB",
    "IATACode": "CKB",
    "ICAOCode": "KCKB",
    "Name": "Benedum Airport"
  },
    {
    "AirportCode": "E95",
    "FAACode": "E95",
    "Name": "Benson Municipal"
  },
    {
    "AirportCode": "ETHE",
    "ICAOCode": "ETHE",
    "Name": "Bentlage Army"
  },
    {
    "AirportCode": "FABB",
    "ICAOCode": "FABB",
    "Name": "Benoni"
  },
    {
    "AirportCode": "FZBE",
    "ICAOCode": "FZBE",
    "Name": "Beno"
  },
    {
    "AirportCode": "FZNS",
    "ICAOCode": "FZNS",
    "Name": "Wageni"
  },
    {
    "AirportCode": "FZVO",
    "ICAOCode": "FZVO",
    "Name": "Beni-Dibele"
  },
    {
    "AirportCode": "GMMD",
    "ICAOCode": "GMMD",
    "Name": "Beni Mellal"
  },
    {
    "AirportCode": "H96",
    "FAACode": "H96",
    "Name": "Benton Municipal"
  },
    {
    "AirportCode": "KBDN",
    "FAACode": "S07",
    "ICAOCode": "KBDN",
    "Name": "Bend Municipal"
  },
    {
    "AirportCode": "KDDH",
    "FAACode": "DDH",
    "ICAOCode": "KDDH",
    "Name": "Morse State"
  },
    {
    "AirportCode": "KVBT",
    "FAACode": "VBT",
    "ICAOCode": "KVBT",
    "Name": "Bentonville Municipal/Thaden"
  },
    {
    "AirportCode": "LFQL",
    "ICAOCode": "LFQL",
    "Name": "Benifontaine"
  },
    {
    "AirportCode": "LKBE",
    "ICAOCode": "LKBE",
    "Name": "Benesov"
  },
    {
    "AirportCode": "M99",
    "FAACode": "M99",
    "Name": "Saline Co/Watts"
  },
    {
    "AirportCode": "O85",
    "FAACode": "O85",
    "Name": "Benton"
  },
    {
    "AirportCode": "Q54",
    "FAACode": "Q54",
    "Name": "Benger"
  },
    {
    "AirportCode": "SNBT",
    "ICAOCode": "SNBT",
    "Name": "Benedito Leite"
  },
    {
    "AirportCode": "TLV",
    "IATACode": "TLV",
    "ICAOCode": "LLBG",
    "Name": "Ben Gurion International Airport"
  },
    {
    "AirportCode": "TUC",
    "IATACode": "TUC",
    "ICAOCode": "SANT",
    "Name": "Benj Matienzo Airport"
  },
    {
    "AirportCode": "XNA",
    "FAACode": "XNA",
    "IATACode": "XNA",
    "ICAOCode": "KXNA",
    "Name": "Northwest Arkansas Regional Airport"
  },
    {
    "AirportCode": "YBDG",
    "ICAOCode": "YBDG",
    "Name": "Bendigo"
  }
]}
