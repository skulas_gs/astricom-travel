﻿var map;
var markersArray = [];

function googleplacesPageShow(){
    ShowLoading();
    var currStartDate = new Date();
    console.log("PageShow: " + currStartDate);
    var googleplacestype = -1;
    if (sessionStorage.getItem("googleplacestype")!= null) {
        googleplacestype = sessionStorage.getItem("googleplacestype");
    }
    getGooglePlaces(googleplacestype);
}

function googleplacedetailsPageShow(){
    ShowLoading();
    if(sessionStorage.ItirenaryDetailsEventID != null)
    {
        GetItirenaryItem();

        $("#googleplaceaddicon").hide();      
        $("#googleplacedeleteicon").show();      

    }
    else
    {
        var googleplaceReference = -1;
	    if (sessionStorage.getItem("id")!= null) {
            googleplaceReference = sessionStorage.getItem("id");
        }    
        getGooglePlaceDetails(googleplaceReference);
        $("#btnMap").hide();  
        
        $("#googleplaceaddicon").show();      
        $("#googleplacedeleteicon").hide();      
              
    }  
 
//     $("#googleplacedetailsTable")
//    .height(
//    $(window).height() - 
//    (70 + $('[data-role=header]').last().height() 
//    + $('[data-role=footer]').last().height())
//    );

}


function googleplacemapPageShow(){
  
    if(sessionStorage.itemmaptype == "googleplace") {
        var googleplacelastresultObj = getGooglePlaceFromSession();    
        getmapbyLocation( googleplacelastresultObj.geometry.location.lat(), 
                         googleplacelastresultObj.geometry.location.lng() );
    }
    else
    {
        getmapbyAddress(sessionStorage.itemmapaddress);
    }     
}

function guidesmapselectPageShow(){
    getguidesmapselect();
}



function getmapbyAddress(address) {

    var geocoder;
    var map;

    geocoder = new google.maps.Geocoder();
    
    
    var currLat;
    var currLong;
	if (sessionStorage.getItem("currLat")!= null) {
        currLat = sessionStorage.getItem("currLat");
       currLong = sessionStorage.getItem("currLong");
    }
    
    var myLoc= new google.maps.LatLng(currLat, currLong)
    
    var myOptions = {
        zoom: 13,
        center: myLoc,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("googleplacemap_canvas"), myOptions);

    geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    
    
         $("#googleplacemap_canvas")
            .height(
            $(window).height() - 
            (5 + $('[data-role=header]').last().height() 
            + $('[data-role=footer]').last().height())
            );
            // tell google to resize the map
            google.maps.event.trigger(map, 'resize');
            
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map, 
                position: results[0].geometry.location
            });
       
    } else {
        alert("Geocode was not successful for the following reason: " + status);
    }
    });
       
}

function getmapbyLocation(currLat, currLong) {

    var myLoc= new google.maps.LatLng(currLat, currLong);
    
    var myOptions = {
      center: myLoc,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleplacemap_canvas"), myOptions);
    
    
    $("#googleplacemap_canvas")
    .height(
    $(window).height() - 
    (5 + $('[data-role=header]').last().height() 
    + $('[data-role=footer]').last().height())
    );
    // tell google to resize the map
    google.maps.event.trigger(map, 'resize');
    
    map.setCenter(myLoc);
    
    var marker = new google.maps.Marker({
      position: myLoc,
      map: map
    });

    map.setCenter(myLoc);
     
}


function getGooglePlaceFromSession()
{
    var googleplacelastresult;
	if (sessionStorage.getItem("googleplacelastresult")!= null) {
        googleplacelastresult = sessionStorage.getItem("googleplacelastresult");
    }

    var googleplacelastresultObj = jQuery.parseJSON(googleplacelastresult);
    var currLat;
    var currLong;
	if (sessionStorage.getItem("googleplacelastresultLat")!= null) {
        currLat = sessionStorage.getItem("googleplacelastresultLat");
       currLong = sessionStorage.getItem("googleplacelastresultLng");
    }
    
    var myLoc= new google.maps.LatLng(currLat, currLong);
    googleplacelastresultObj.geometry.location = myLoc;
    return googleplacelastresultObj;
}

function setGooglePlaceToSession(googleplaceObj)
{
    sessionStorage.googleplacelastresult = JSON.stringify(googleplaceObj);
    sessionStorage.googleplacelastresultLat = googleplaceObj.geometry.location.lat();
    sessionStorage.googleplacelastresultLng = googleplaceObj.geometry.location.lng()
}

function getGooglePlaceDetails(googleplaceReference)
{
    var currLat;
    var currLong;
	if (sessionStorage.getItem("currLat")!= null) {
        currLat = sessionStorage.getItem("currLat");
       currLong = sessionStorage.getItem("currLong");
    }
    
     var myLoc= new google.maps.LatLng(currLat, currLong)


    var request = {
      reference: googleplaceReference
    };

    service = new google.maps.places.PlacesService(map);
    service.getDetails(request, populateGooglePlaceDetails);

}
  
function populateGooglePlaceDetails(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        
        $('#googleplacedetailsList').html("");
        var triptems = [1];
        triptems[0] = results;    	    
        
        $('#googleplaceTitle').html(capitaliseFirstLetter(results.types[0]) + ' Info');
        
       
        
        name = results.name;
	    link = results.website;
	    picture = 'http://www.gallopstars.co.il/App_Themes/Default/images/AppLogo_ShareImage.png';
	    caption = results.name;
	    description = results.name + ' at: ' + results.formatted_address;
        
        setFacebookDetailsToSession(name, link, picture, caption, description);
         
         
        sessionStorage.itemmaptype = 'googleplace';
        sessionStorage.itemmapaddress = ''; 
         
        $( "#googleplacedetails" ).tmpl( triptems  ,
                { 
                    GetIcon: GetIcon
                }
                ).appendTo( "#googleplacedetailsList" );
    		            
        setGooglePlaceToSession(results);

                   
    }
    HideLoading();
}

function capitaliseFirstLetter(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getGooglePlaces(googleplacestype)
{
    var currLat;
    var currLong;
	if (sessionStorage.getItem("currLat")!= null) {
        currLat = sessionStorage.getItem("currLat");
       currLong = sessionStorage.getItem("currLong");
    }
    
     var myLoc= new google.maps.LatLng(currLat, currLong)

    map = new google.maps.Map(document.getElementById("googleplacescanvas"), {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: myLoc,
      zoom: 13
    });

    var request = {
      location: myLoc,
      radius: 5000,
      keyword: GetGooglePlaceKeywordByType(googleplacestype)
    };
    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.search(request, populateGooglePlaces);
 }

function populateGooglePlaces(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        $('#googleplaceslist li').remove();
	    triptems = results;
		
	    $( "#googleplacesitem" ).tmpl( triptems ,
                { 
                    GetIcon: GetIcon
                }
                ).appendTo( "#googleplaceslist" );

	    $('#googleplaceslist').listview('refresh');
    }
    HideLoading();
}

function GetIcon()
{
    return GetImageByEventType(sessionStorage.googleplacestype);   
}    

function GetGooglePlaceKeywordByType(type)
{
    switch(type.toLowerCase()) 
    {   
        case 'hotel':
            return 'hotel';
            break;        
        case 'eat & drink':       
            return 'food';
            break;                     
        case 'landmarks':    
            return 'history';
            break;        
        case 'shop':    
            return 'mall';
            break;
    }
}



function showgoogleplaceMoreInfo() {
   var googleplaceurl = $("#googleplacesURL").val();
   
   if(isOnMobile)
       window.plugins.childBrowser.showWebPage(googleplaceurl + "&hl=en", { showLocationBar: false });
   else
       $('#googleplacedetailsList').load(googleplaceurl + "&hl=en");   
}





function getguidesmapselect() {


    var currLat;
    var currLong;
	if (sessionStorage.getItem("currLat")!= null) {
        currLat = sessionStorage.getItem("currLat");
       currLong = sessionStorage.getItem("currLong");
    }
    
    var myLoc= new google.maps.LatLng(currLat, currLong)
    
    var mapOptions = {
        center: myLoc,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    map = new google.maps.Map(document.getElementById('guidesmapselect_canvas'),
      mapOptions);

    placeMarker(myLoc);
    
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
      });

    var input = document.getElementById('searchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    autocomplete.setTypes([]);
    
    mapresize();
    
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      map: map
    });
    markersArray.push(marker);
    
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      infowindow.close();
      var place = autocomplete.getPlace();
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }

      var image = new google.maps.MarkerImage(
          place.icon,
          new google.maps.Size(71, 71),
          new google.maps.Point(0, 0),
          new google.maps.Point(17, 34),
          new google.maps.Size(35, 35));
      marker.setIcon(image);
      marker.setPosition(place.geometry.location);
      
      saveCurrPos(place.geometry.location.lat(), place.geometry.location.lng());
      
      var address = '';
      if (place.address_components) {
        address = [(place.address_components[0] &&
                    place.address_components[0].short_name || ''),
                   (place.address_components[1] &&
                    place.address_components[1].short_name || ''),
                   (place.address_components[2] &&
                    place.address_components[2].short_name || '')
                  ].join(' ');
      }

//      infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
//      infowindow.open(map, marker);
      
      //mapresize();
       
    });       
}

function deleteOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}

function placeMarker(location) {
    deleteOverlays();
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });

    map.setCenter(location);
    markersArray.push(marker);
    saveCurrPos(location.lat(), location.lng());
}

function mapresize()
{
    $("#guidesmapselect_canvas")
    .height(
    $(window).height() - 
    (155 + $('[data-role=header]').last().height()  +  $("#searchTextField").last().outerHeight())
    );
    // tell google to resize the map
    google.maps.event.trigger(map, 'resize');
}

function setcurrgooglemapposition()
{
  OpenPage('guidesmainmenuPage');
}
   

