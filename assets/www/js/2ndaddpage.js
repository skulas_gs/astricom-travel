function nextAddPage(){
    sessionStorage.setItem("address",document.getElementById("address-panel").innerHTML);

    $.mobile.sdCurrentDialog.updateBlank('<center><table style="vertical-align:middle; width:80%;">' +
                                         '<tr><td align="center" valign="top">' +
                                         '<h4 style="color:DodgerBlue;">Add Details</h4>' +
                                         '</td></tr>' +
                                         '<tr><td align="center" valign="top"><input id="title" type="text" placeholder="Title" /></td></tr>' +
                                         '<tr><td align="center" valign="top">' +
                                         '<textarea id="description" class="message" maxlength="150" rows="10" cols="30" placeholder="Description" '+
                                         'onchange="updateCountdown()" onkeyup="updateCountdown()"></textarea></td></tr>' +
                                         '<tr><td align="right"><span class="countdown"></span></td></tr>' +
                                         '<tr><td align="center"><button rel="close" class="small-button" data-inline="true" onClick="saveLocation();" >Save</button></td>' +
                                         '</tr></table></center>');
    updateCountdown();
}

function saveLocation() {
    SaveCurrNavPosGuy();
    //alert("saving position");
	rememberPosition(document.getElementById('title').value, document.getElementById('description').value, 
                     sessionStorage.getItem('id'), sessionStorage.getItem("image"),
                     sessionStorage.getItem("currLat"),sessionStorage.getItem("currLong"),false);
	var db = GetDB();
	//db.transaction(countDB);
    db.transaction(insertDB, errorCB, successInsert);
}

function insertDB(tx){
	var fields = ["title","description","lat","long","address"];
	var values = [sessionStorage.getItem("title"),sessionStorage.getItem("description"),  
	              sessionStorage.getItem("currLat"),sessionStorage.getItem("currLong"),sessionStorage.getItem("address")];
	var select = buildQuery("INSERT", "LOCATIONS", fields, values, "");	
    //alert(select);
    //alert("executing insert query");
    tx.executeSql(select);
}

function successInsert() {
    //alert("insert successful");
	var x = sessionStorage.getItem("number");
	x++;
	sessionStorage.setItem("number", x);
	OpenPage('findmycarPage');
}
