﻿function directionsPageShow(){
                           sessionStorage.setItem("mode","DRIVING");
                           getDirections();
}

function locationPageShow(){
                        getLocation();
                        SaveCurrNavPosGuy();
                          }


 
    var x=document.getElementById("demo");
    
    var currRef;

    
    function getLocation()
      {
      if (navigator.geolocation)
        {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
        }
      else{x.innerHTML="Please enable location services.";}
      }
      
     function getPlaceList()
      {
      if (navigator.geolocation)
        {
        navigator.geolocation.getCurrentPosition(LoadPlaceList,showError);
        }
      else{x.innerHTML="Please enable location services";}
      }
  
     function getPlaceDetails(referenceID)
      {
      currRef = referenceID;
      if (navigator.geolocation)
        {
        navigator.geolocation.getCurrentPosition(LoadPlaceDetails,showError);
        }
      else{x.innerHTML="Please enable location services";}
      }
 
 
      
     function getDirections()
      {
      if (navigator.geolocation)
        {
    	  
    	  navigator.geolocation.getCurrentPosition(showDirections,showError);
        }
      else{x.innerHTML="Please enable location services";}
      }
           
    
function saveCurLocation(){
    
    if (navigator.geolocation)
    {        
        navigator.geolocation.getCurrentPosition(savePosition,showError);
    }
    else
    {
        x.innerHTML="Geolocation is not supported by this browser.";
    }
}     
    

    function SaveCurrNavPos()
    {
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(savePosition,showError);
        }
        else
        {
            x.innerHTML="Please enable location services";
        }
    }

function SaveCurrNavPosGuy()
{
    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(savePositionGuy,showError);
    }
    else
    {
        x.innerHTML="Please enable location services";
    }
}

function savePositionGuy(position)
{
    //alert("ciao");
    var myLoc= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    var gc = new google.maps.Geocoder();
    gc.geocode({'latLng': myLoc}, function(results, status) {
               if (status == google.maps.GeocoderStatus.OK)
               {
                if (results[0]) 
                {
                    console.log(results[0].formatted_address);
                    sessionStorage.setItem("address", results[0].formatted_address);
                    document.getElementById("address-panel").innerHTML = results[0].formatted_address;
                }
               } 
               else
               {
                //alert("Geocoder failed due to: " + status);
                document.getElementById("address-panel").innerHTML = "Location not available";
               }
               });
    
    savePosition(position);
}
   
   
    function savePosition(position)
    {
        sessionStorage.setItem("currLat", position.coords.latitude);
        sessionStorage.setItem("currLong", position.coords.longitude);
    }

    function saveCurrPos(currLat, currLong) {
        sessionStorage.setItem("currLat", currLat);
        sessionStorage.setItem("currLong", currLong);
    }
    
     function showPosition(position)
      {
          var myLoc= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          var myOptions = {
            center: myLoc,
            zoom: 15,
            zoomControl: true,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
          var image = "pics/CarIcon.png";
          var marker = new google.maps.Marker({
                                              position: myLoc,
                                              map: map,
                                              title:"Hello World!",
                                              icon: image
                                              });          
          SaveCurrNavPosGuy();
      }
      
      
    var map;
    var infowindow;
    
    
    
    
    function LoadPlaceList(position)
      {     
        

       var myLoc= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        map = new google.maps.Map(document.getElementById("PlacesListCanvas"), {
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: myLoc,
          zoom: 13
        });

        var request = {
          location: myLoc,
          radius: 5000,
          keyword: 'history' 
        };
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.search(request, PopulatPlaceList);
      }

      function PopulatPlaceList(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            $('#PlacesList li').remove();
		    triptems = results;
    		
		    $( "#PlacesListItem" ).tmpl( triptems )
                        .appendTo( "#PlacesList" );

		    $('#PlacesList').listview('refresh');
        }
      }
    
    
    
    function LoadPlaceDetails(position)
      {     
        
       var myLoc= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

       var request = {
          reference: currRef
        };

        service = new google.maps.places.PlacesService(map);
        service.getDetails(request, PopulatPlaceDetails);
        
      }
          
      function PopulatPlaceDetails(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            
            $('#PlaceDetailsList').html("");
            var triptems = [1];
		    triptems[0] = results;    	    

		    $( "#PlaceDetails" ).tmpl( triptems )
            		        .appendTo( "#PlaceDetailsList" );
            //$('#PlaceDetailsList').load(results.url + "&hl=en");
	
            //window.plugins.childBrowser.showWebPage(results.url + "&hl=en", { showLocationBar: false });
                       
        }
      }
    


     
 // Directions functions
    function showDirections(position)
      {
    	 var directionDisplay;
         var directionsService = new google.maps.DirectionsService();
         
         var tm = sessionStorage.getItem("mode");
          
          var myLoc= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          var loc = new google.maps.LatLng(sessionStorage.getItem('locLat'),sessionStorage.getItem('locLong'));

          directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true});
        var myOptions = {
          mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: true,
        streetViewControl: false,
          center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
        };
        var map = new google.maps.Map(document.getElementById('directionsmap_canvas'),
            myOptions);
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('directions-panel'));

        var image = "pics/CarIcon.png";
          var marker = new google.maps.Marker({
                                              position: myLoc,
                                              map: map,
                                              title:"Origin"
                                              });
          
          var marker2 = new google.maps.Marker({
                                              position: loc,
                                              map: map,
                                               title:"Destination",
                                               icon: image
                                              });

        var request = {
          origin: myLoc,
          //destination: new google.maps.LatLng(32.083756,34.808185),
      	  destination: loc,        		
          travelMode: tm
        };
        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
          }
        });
     
      }

    
    
    //Other
    function showError(error)
      {
      switch(error.code) 
        {
        case error.PERMISSION_DENIED:
          alert("User denied the request for Geolocation.");
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Location information is unavailable.");
          break;
        case error.TIMEOUT:
          alert("The request to get user location timed out.");
          break;
        case error.UNKNOWN_ERROR:
          alert("An unknown error occurred.");
          break;
        }
      }
    
    function directions()
    {
    	OpenPage('directionsPage');
    }
    
    function changeMode(){
    	sessionStorage.setItem("mode",document.getElementById("mode").value);
    }
    
/*    function calcRoute() {
        var directionsService = new google.maps.DirectionsService();
    	  var selectedMode = document.getElementById("mode").value;
    	  var request = {
    	      origin: new google.maps.LatLng(sessionStorage.getItem['currLat'],sessionStorage.getItem['currLong']),
    	      destination: new google.maps.LatLng(sessionStorage.getItem['locLat'],sessionStorage.getItem['locLong']),
    	      // Note that Javascript allows us to access the constant
    	      // using square brackets and a string value as its
    	      // "property."
    	      travelMode: google.maps.TravelMode[selectedMode]
    	  };
    	  directionsService.route(request, function(response, status) {
    	    if (status == google.maps.DirectionsStatus.OK) {
    	      directionsDisplay.setDirections(response);
    	    }
    	  });
    	}*/
