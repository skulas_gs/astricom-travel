﻿

function itineraryPageShow(){
    GetItirenaryList();
}
  

function additineraryeventtypePageShow(){

    var PlaceArr = new Array("Flight","Hotel","Car","Train","Bus","Eat & Drink","Shop","Landmarks","Museums","Nature","Shows","Sports","Special Events","Other");

    var PlaceArrObj={}; 
    for (var j = 0; j < PlaceArr.length; j++) {
        PlaceArrObj[PlaceArr[j]] = PlaceArr[j];
    }
    var PlaceWheel = [{}];    
    PlaceWheel[0]['Places'] = PlaceArrObj;


    $('#itineraryeventtypeinput').scroller({
        theme: 'android-ics',
        display: 'inline',
        mode: 'scroller',
        wheels: PlaceWheel
    });
           
    var myeventType = sessionStorage.EventType;
    if(myeventType != null)
    {                         
        $("#itineraryeventtypeinput").val(myeventType);
    
        $('#itineraryeventtypeinput').scroller('setValue', [myeventType]);
    }
    else    
        $("#itineraryeventtypeinput").val(PlaceArr[0]);
        
    $(".dwc").css('float', 'none');
}


function additinerarystartdatePageShow(){
   
    var now = new Date();
    
    $('#itinerarystartdateinput').scroller({
        preset: 'datetime',
        theme: 'android-ics',
        width: 10,
        minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
        display: 'inline',
        mode: 'scroller'
    });
    

    $("#itinerarystartdateinput").val(jQuery.scroller.formatDate('yyyy-MM-dd HH:mm:ss', new Date()));


}



function additinerarydetailsPageShow(){
   
   $("#itirinaerydetailsName").val(sessionStorage.Name);
   $("#itirinaerydetailsDesc").val(sessionStorage.Description);
   $("#itirinaerydetailsPlace").val(sessionStorage.Place);
   $("#itirinaerydetailsAddress").val(sessionStorage.Address);
   $("#itirinaerydetailsPhone").val(sessionStorage.Phone);
}



function userdefinedplacedetailsPageShow(){   
    if(sessionStorage.ItirenaryDetailsEventID != null)
    {   
        ShowLoading();
        GetItirenaryItem();
        $("#userdefinedaddicon").hide();
        $("#userdefineddeleteicon").show();
    }
}

  
  
function populateUserDefinedDetails(currTime, myItem)
{

//  var userdefinedObj =  { "EventID" : '4', "EventType" : 'Car', "Name" : 'MyName', "Description" : 'My Desc',
//     "ImageURL" : GetImageByEventType('car'), "Time" : '24-05-2012', "Place" : 'My Place', "Address" : 'My Address', "Phone" : '124552345' };
//   
 
    var userdefinedObj =  { "EventID" : myItem.EventID, "EventType" : myItem.EventType, "Name" : myItem.Name, "Description" : myItem.Description,
     "ImageURL" : GetImageByEventType(myItem.EventType), "Time" : currTime, "Place" : myItem.Place, "Address" : myItem.Address, "Phone" : myItem.Phone };
   
   
    name = myItem.Name;
    link = '';
    picture = 'http://www.gallopstars.co.il/App_Themes/Default/images/AppLogo_ShareImage.png';
    caption = myItem.Place;
    description = myItem.Description;
    
    setFacebookDetailsToSession(name, link, picture, caption, description);


    sessionStorage.itemmaptype = 'userdefinedplace';
    sessionStorage.itemmapaddress = myItem.Address; 

    $("#userdefineddetailsList").html("");
    $('#userdefineddetailsTitle').html(capitaliseFirstLetter(myItem.EventType) + ' Info');
     $( "#userdefineddetails" ).tmpl( userdefinedObj )
            .appendTo( "#userdefineddetailsList" );

}
   
function SetItineraryEventType()
{
    var EventType = $("#itineraryeventtypeinput").val().toLowerCase();
    if(EventType == "flight")
        OpenPage('flightsearchdepparturePage');  
    else
    {
        sessionStorage.EventType= $("#itineraryeventtypeinput").val().toLowerCase();
        OpenPage('additinerarystartdatePage');
    }
}

function SetItineraryStartDate()
{
    var d = new Date($("#itinerarystartdateinput").val());
    sessionStorage.StartDate = d.getTime();
    
    OpenPage('additinerarydetailsPage');  
}


//Start add wizard
function AddToItinerary(isGooglePlace)
{
    if(isGooglePlace)
    {
        var googleplacelastresultObj = getGooglePlaceFromSession();
                           
        sessionStorage.EventSetID = 0;
        sessionStorage.EventType = sessionStorage.getItem("googleplacestype"); 
        sessionStorage.Name = googleplacelastresultObj.name;
        sessionStorage.EventLat = sessionStorage.getItem("googleplacelastresultLat");
        sessionStorage.EventLng = sessionStorage.getItem("googleplacelastresultLng");
        sessionStorage.DataJSon = sessionStorage.getItem("googleplacelastresult");
        sessionStorage.Description = googleplacelastresultObj.html_attributions[0];
        sessionStorage.Place = googleplacelastresultObj.vicinity;
        sessionStorage.Address = googleplacelastresultObj.formatted_address;
        sessionStorage.Phone = googleplacelastresultObj.formatted_phone_number;
        sessionStorage.Email = "";
        sessionStorage.Contact = "";
    }
    else
    {
        DeleteItirenaryItemInSession();       
    }
    
    OpenPage('additineraryeventtypePage');
}

function DeleteItirenaryItemInSession()
{

    sessionStorage.removeItem("ItirenaryDetailsEventID");
 
    sessionStorage.removeItem("EventID");
	sessionStorage.removeItem("EventSetID");
    sessionStorage.removeItem("EventType");
    sessionStorage.removeItem("EventLat");
    sessionStorage.removeItem("EventLng");
    sessionStorage.removeItem("Name");
    sessionStorage.removeItem("StartDate");
    sessionStorage.removeItem("EndDate");
    sessionStorage.removeItem("DataJSon");
    sessionStorage.removeItem("Description");
    sessionStorage.removeItem("Place");
    sessionStorage.removeItem("Address");
    sessionStorage.removeItem("Phone");
    sessionStorage.removeItem("Email");
    sessionStorage.removeItem("Contact");
}

//Add Data to DB
function AddItirenaryItem()
{
    sessionStorage.EventSetID = 0;
    //sessionStorage.EventType = "flight"; // Set already
    sessionStorage.Name = $("#itirinaerydetailsName").val();
    //sessionStorage.StartDate = d.getTime(); // Set already
    sessionStorage.EndDate = sessionStorage.StartDate;
    //sessionStorage.DataJSon = "";
    sessionStorage.Description = $("#itirinaerydetailsDesc").val();
    sessionStorage.Place = $("#itirinaerydetailsPlace").val();
    sessionStorage.Address = $("#itirinaerydetailsAddress").val();
    sessionStorage.Phone = $("#itirinaerydetailsPhone").val();
    sessionStorage.Email = "";
    sessionStorage.Contact = "";
    
    InsertItirenaryItem();
    
    OpenPage('itineraryPage');
}


function FilterItirenary(btnFilter, filter) {

//    $('#itirenaryFilters').find('input').each(function() {
//        var currentinput = $(this);
//        currentinput[0].src = currentinput[0].src.replace('Enabled', 'Disabled');
//    });
//    
//    btnFilter.src = btnFilter.src.replace('Disabled', 'Enabled');

    $("#itirenarylist li").each(function(){
           $(this).show();
    });
    
    
    var SEARCHWORD = filter;
    $("#itirenarylist li").each(function(){
        if($(this).attr("data-filtertext") != null)
        {
            if($(this).attr("data-filtertext").toUpperCase().indexOf(SEARCHWORD.toUpperCase()) >=0)
               $(this).show();
            else
               $(this).hide();
        }
    });
    $('#itirenarylist').listview('refresh');
}

function GetFilterByEventType(EventType)
{
    switch(EventType.toLowerCase()) 
    {    
        case 'flight':
            return 'flight';
            break;
        case 'hotel':
            return 'hotel';
            break;
        case 'car':
        case 'train':
        case 'bus':
            return 'transport';
            break;
        case 'eat & drink':
        case 'shop':
        case 'landmarks':
        case 'museums':
        case 'nature':
        case 'shows':
        case 'sports':
        case 'special events':
        case 'other':
            return 'other';
            break;
        default:
            return '';
    }
}



function GetImageByEventType(EventType)
{   
    console.log('Get image by EventType : ' + EventType);
    switch(EventType.toLowerCase()) 
    {    
        case 'flight':
            return 'pics/PlaneIcon.png';
            break;
        case 'hotel':
            return 'pics/Itinerary-HotelIcon.png';
            break;
        case 'car':
            return 'pics/CarIcon.png';
            break;
        case 'train':
            return 'pics/TrainIcon.png';
            break;
        case 'bus':
            return 'pics/BusIcon.png';
            break;
        case 'eat & drink':
            return 'pics/RestIcon.png';
            break;
        case 'shop':
            return 'pics/ShopIcon.png';
            break;
        case 'landmarks':
            return 'pics/SeeIcon.png';
            break;
        case 'museums':
            return 'pics/MuseumIcon.png';
            break;
        case 'nature':
            return 'pics/NatureIcon.png';
            break;
        case 'shows':
            return 'pics/ShowsIcon.png';
            break;
        case 'sports':
            return 'pics/SportsIcon.png';
            break;
        case 'special events':
            return 'pics/SpecialEventIcon.png';
            break;
        case 'other':
            return 'pics/PlaneIcon.png';
            break;
        default:
             return 'pics/PlaneIcon.png';
    }
}