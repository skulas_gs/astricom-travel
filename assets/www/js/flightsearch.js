﻿
         
function flightsearchdepparturePageShow(){
    $("#deppartureInput").on("input", GetAirportList);
     
    $("#flightsearchdepparturePage").find('.ui-input-clear').live('click', function(e){
        $('#depparturesuggestions li').remove();
        $('#depparturesuggestions').listview("refresh");        
        $("#depparturelistmessage").html(typetostart);
    });
}

function flightsearcharivalPageShow(){
    $("#arivalInput").on("input", GetAirportList);

    $("#flightsearcharivalPage").find('.ui-input-clear').live('click', function(e){
        $('#arivalsuggestions li').remove();
        $('#arivalsuggestions').listview("refresh");        
        $("#arivallistmessage").html(typetostart);
    });
}

function flightdetailsPageShow(){
    
    ShowLoading();
    if(sessionStorage.ItirenaryDetailsEventID != null)
    {
        GetItirenaryItem();
        $("#flightaddicon").hide();      
        $("#flightdeleteicon").show();      
    }
    else
    {
        if (sessionStorage.getItem("flightJson")!= null) {
            flightJson = sessionStorage.getItem("flightJson");
            PopulateFlightDetails(flightJson);
        }  
        $("#flightaddicon").show();      
        $("#flightdeleteicon").hide();      
  
    }        
    
}

function flightsearchdatePageShow(){

 var now = new Date();
    
    $('#flightdateinput').scroller({
        preset: 'date',
        theme: 'android-ics',
        width: 10,
        minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
        display: 'inline',
        mode: 'scroller'
    });
    

    $("#flightdateinput").val(jQuery.scroller.formatDate('yyyy-MM-dd HH:mm:ss', new Date()));
    
    $(".dwc").css('float', 'none');
}

function flightlistPageShow(){
    ShowLoading();
    ShowFlightList();
}


function PopulateFlightDetails(flightJson)
{        
    var flightJsonObj = jQuery.parseJSON(flightJson);
       
    name = flightJsonObj.Airline.Name + ' flight to ' + flightJsonObj.Destination.Name;
    link = '';
    picture = 'http://www.gallopstars.co.il/App_Themes/Default/images/AppLogo_ShareImage.png';
    caption = flightJsonObj.Airline.Name + ' flight to ' + flightJsonObj.Destination.Name;
    description = flightJsonObj.Airline.Name + ' flight to ' + flightJsonObj.Destination.Name +
                                 ' at ' + flightJsonObj.ScheduledGateDepartureDate.replace("T"," ").replace(new RegExp(":......" + '$'), '');
    
    setFacebookDetailsToSession(name, link, picture, caption, description);
  
    sessionStorage.itemmaptype = 'flightplace';
    sessionStorage.itemmapaddress = flightJsonObj.Origin.Name; 

    $('#flightdetailsList').html("");
    
    $( "#flightdetails" ).tmpl( flightJsonObj,
        { 
            GetDate: GetFlightDate,
            GetTime: GetFlightTime,
            GetIcon: GetIcon
        }
     ).appendTo( "#flightdetailsList" );
     HideLoading();
}

function AddFlightToItinerary()
{
    if (sessionStorage.getItem("flightJson")!= null) {
        flightJson = sessionStorage.getItem("flightJson");
    }
    var flightJsonObj = jQuery.parseJSON(flightJson);
    
    var d = new Date(flightJsonObj.ScheduledGateDepartureDate.replace("T"," "));
    
    sessionStorage.EventSetID = 0;
    sessionStorage.EventType = "flight";
    sessionStorage.EventLng = "";
    sessionStorage.EventLat = "";
    sessionStorage.StartDate = d.getTime();
    sessionStorage.EndDate = d.getTime();
    sessionStorage.DataJSon = flightJson;
    sessionStorage.Description = "";
    sessionStorage.Place = "";
    sessionStorage.Address = "";
    sessionStorage.Phone = "";
    sessionStorage.Email = "";
    sessionStorage.Contact = "";
 
    InsertItirenaryItem();
            
    OpenPage('itineraryPage');
}



function GetAirportList(e) {
    var hiddencount;
    var setfunc;
    var prefX = "";
    if( $(this).attr('id') == "deppartureInput")
    {
        setfunc = "SetFlightDeparture";
        prefX = 'depparture';
    }
    else
    {
        setfunc = "SetFlightArival";
        prefX = 'arival';
    }
        
        
    var sugList = $("#" + prefX + "suggestions");  

    var text = $(this).val();
    if(text.length < 3)
    {
        $("#" + prefX + "listmessage").html(typetostart);
        $("#" + prefX + 'suggestions li').remove();
    }
    else
    {       
        hiddencount = 0;
        if(text.length == 3 && $(    + prefX + "suggestions li").length == 0 )
        {   
            $("#" + prefX + 'suggestions li').remove();
             
            var AirportServiceSearch = GetAirportService(text);

            if(!isOnMobile)
            AirportServiceSearch = "js/airports.js"

            $.ajax({
            dataType: "html",
            url: AirportServiceSearch,
            success: function( data ) {
                console.log(data);
                airports = data;
                airports = jQuery.parseJSON(data); 
                airports = airports.Airport;
                
                if( airports != null && airports.length == null)
                 airports = [airports];

                $.each(airports, function(index, airport) {                                
                    sugList.append('<li data-filtertext="' + airport.Name + '">' +
                    '<a href="#" onclick="' + setfunc + '(\'' + airport.AirportCode + '\');">' 
                    + airport.Name + ' (' + airport.AirportCode + ')' + '</a></li>');
                }); 
                
                sugList.listview("refresh");
                CheckEmptyAirportList();
                HideLoading();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('There was an error loading data.');
                }
            });
        }
        else
	    {			    
	        $("#" + prefX + "suggestions li").each(function(){
                if($(this).attr("data-filtertext").toUpperCase().indexOf(text.toUpperCase()) >=0)
                   $(this).show();
                else
                {
                    hiddencount++;
                    $(this).hide();
                }
            });
            sugList.listview("refresh");
            CheckEmptyAirportList();
	    }	    	    
    }
    
    function CheckEmptyAirportList(){
    if($("#" + prefX + "suggestions li").length == 0 || $("#" + prefX + "suggestions li").length == hiddencount)    
         $("#" + prefX + "listmessage").html(noairportresult);
    else
        $("#" + prefX + "listmessage").html("");
}  

}

function SetFlightDeparture(flightsearchdep)
{
    sessionStorage.flightsearchdep= flightsearchdep;
    OpenPage('flightsearcharivalPage');  
}

function SetFlightArival(flightsearchari)
{
    sessionStorage.flightsearchari= flightsearchari;
    OpenPage('flightsearchdatePage');  
}

function SetFlightDate()
{
    var startdate = new Date($("#flightdateinput").val());
    setMonth = startdate.getMonth() + 1;
    sessionStorage.flightsearchtime = startdate.getFullYear() + "-" + setMonth + "-" + startdate.getDate();
    OpenPage('flightlistPage');  
}

function SetFlightDetails()
{
    sessionStorage.flightJson= $("#flightJson").val(); 
}

function ShowFlightList()
{
    var startdate;
    var enddateSTR;
    var depCode;
    var ariCode;
	if (sessionStorage.getItem("flightsearchtime")!= null) {
        startdate = sessionStorage.getItem("flightsearchtime");
        depCode = sessionStorage.getItem("flightsearchdep");
        ariCode = sessionStorage.getItem("flightsearchari");
    }
    
    var startth = startdate.split("-")
    var enddate = new Date(startth[0], startth[1], startth[2]);
    enddate.setDate(enddate.getDate()+1);
    enddateSTR = enddate.getFullYear() + "-" + enddate.getMonth() + "-" + enddate.getDate();    
 
    
    var FlightsService = GetFlightsService(startdate, enddateSTR, depCode, ariCode);
    
    if(!isOnMobile)
        FlightsService = "js/flights.js"
    
    console.log(FlightsService);
    $.ajax({
		dataType: "html",
        url: FlightsService,
		success: function( data ) {
		    console.log(data);
		    flighthistory = data;
            flighthistory = jQuery.parseJSON(data); 
            flightitems = flighthistory.FlightHistory;
            
            $('#flightlist li').remove();
    		
		    $( "#flightitem" ).tmpl( flightitems,
		        { 
                    GetDate: GetFlightDate
                }
             ).appendTo( "#flightlist" );

		    $('#flightlist').listview('refresh');
		     var i =0;
		    $('#flightlist').find('li').each(function(){
                var currentli = $(this);
                var currenthref =  currentli.find('a');
                currenthref.append("<input type='hidden' name='flightJson' id='flightJson' value='" + JSON.stringify(flightitems[i])+ "'/>");
                i++;                
            });
            
            if(i > 0)
                $("#listmessage").html("");
            else
                $("#listmessage").html(noflightresult);
            HideLoading();
		}
	});
}

function GetFlightDate(flighttype) {
    
    if(flighttype == "Ari") 
    {                
        return this.data.ScheduledGateArrivalDate.replace("T"," ").replace(new RegExp(":......" + '$'), '');
    }
    else   
    {
        return this.data.ScheduledGateDepartureDate.replace("T"," ").replace(new RegExp(":......" + '$'), '');
    }
}

function GetFlightTime() {
    var numerator = parseInt(this.data.ScheduledBlockTime);
    var denominator = 60;
    
    var remainder = numerator % denominator;
    var quotient = ( numerator - remainder ) / denominator;

    return quotient + " Hrs. " + remainder + " Min.";
}
