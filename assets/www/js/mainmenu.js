﻿

function morePageShow(){
	//var name = sessionStorage.getItem("userName");
    //$('#userWelcome').html("Welcome " + name);
}


function settingsPageShow(){
	$("input[type='radio']").bind( "change", function(event, ui) {
        alert(this.value);
    });
    
    
    $("#updateselector").bind( "change", function(event, ui) {
     alert('Value change to ' + $(this).attr('value'));
    });   
    
    $("#btnUpdateCSS").bind( "click", UpdateCSS);
          

}

function feedbackPageShow(){
   $("#feedbackSend").bind( "click", function(event, ui) {
     //alert('Value change to ' + $(this).attr('value'));
     alert($("#feedbackSubj").val() + ' ' + $("#feedbackDesc").val());
    }); 
}


function inboxPageShow(){
    var id = -1;
	if (sessionStorage.getItem("userID")!= null) {
        id = sessionStorage.getItem("userID");
    }
	gettripitems(id);
}


function inboxmessagePageShow(){
	var id = getUrlVars()["id"];
	//gettripdetails(id);
}



function guidesmainmenuPageShow(){
    var searchnearcity = "";
	if (sessionStorage.getItem("searchnearcity")!= null) {
        searchnearcity = sessionStorage.getItem("searchnearcity");
        $("#guidesmainmenuheader").html($("#guidesmainmenuheader").html() + ' ' + searchnearcity);
    }
	   
}


function weatherPageShow(){
    ShowLoading();
    ShowWeather();
}


function ShowWeather()
{

    var currLat;
    var currLong;
	if (sessionStorage.getItem("currLat")!= null) {
        currLat = sessionStorage.getItem("currLat");
       currLong = sessionStorage.getItem("currLong");
    }
    
    var weatherserviceURL = GetweatherserviceURL(currLat, currLong);
    var weatheritems = {};



    $.get(weatherserviceURL, {}, function(res,code) {
	        //$('#weatherlist li').remove();
		    weatheritems = res.data.weather;
    		
//		    $( "#weatheritem" ).tmpl( weatheritems ,
//                { 
//                    GetDate: GetWeatherDate,
//                    GetWeatehrIcon: GetWeatehrIcon,
//                    GetWeatherDesc: GetWeatherDesc
//                }
//             ).appendTo( "#weatherlist" );

            for(var i=0, len=weatheritems.length; i<len; i++) {
                    if(weatheritems[i] != "")
                    {
                        var k = i + 1;
                        $('#weatherimage' + k).attr('src', GetWeatehrIcon(weatheritems[i].weatherCode) );
                        $('#weatherdate' + k).html(GetWeatherDate(weatheritems[i].date));
                        $('#weathertemp' + k).html(weatheritems[i].tempMaxC + "°C (" + weatheritems[i].tempMaxF + "°F)");
                        $('#weatherdesc' + k).html(weatheritems[i].weatherDesc[0].value);
                    }
             }

		    //$('#weatherlist').listview('refresh');
		    
		    HideLoading();
		    },"jsonp");
		     
}


function GetWeatherDesc() {
    return this.data.weatherDesc[0].value;
}

function GetWeatherDate(date) {    
   var currDate = new Date(date);
   

    var weekday=new Array(7);
    weekday[0]="Sunday";
    weekday[1]="Monday";
    weekday[2]="Tuesday";
    weekday[3]="Wednesday";
    weekday[4]="Thursday";
    weekday[5]="Friday";
    weekday[6]="Saturday";

    var DayOfWeek = weekday[currDate.getDay()];
    
    return DayOfWeek + ', ' + date;
}

function GetWeatehrIcon(weatherCode) {    

    switch(weatherCode) 
    {    
        case '395':
        case '392':
        case '389':
        case '386':
        case '377':
        case '374':
        case '371':
        case '368':
        case '365':
        case '362':
        case '359':
        case '356':
        case '353':
        case '350':
        case '338':
            return 'pics/WeatherIcons/CloudyIcon.png';
            break;        
        case '335':
        case '332':
        case '329':
        case '326':
        case '323':
        case '320':
        case '317':
        case '314':
        case '311':
        case '308':
        case '305':
        case '302':
        case '299':
        case '296':
        case '293':
        case '284':
        case '281':
        case '266':
        case '263':       
            return 'pics/WeatherIcons/PartlyCloudyIcon.png';
            break;        
        
            return 'pics/WeatherIcons/RainyIcon.png';
            break;        
        case '260':
        case '248':
        case '230':
        case '227':
        case '200':
        case '185':
        case '182':
        case '179':
        case '176':        
            return 'pics/WeatherIcons/SnowIcon.png';
            break;        
        case '143':
        case '122':
        case '119':
        case '116':
        case '113':      
            return 'pics/WeatherIcons/SunnyIcon.png';
            break;
    }
}    
        
//UpdateCSS functions




var CSSLocalPath = "";
            
    function UpdateCSS(){
          window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, getCSSDirectory, fail);         
    }


    function getCSSDirectory(fileSystem) {
        CSSLocalPath = fileSystem.root.fullPath + '/truvle/testtheme2.min.css';
        fileSystem.root.getDirectory("truvle", {create: true, exclusive: false}, getTruvleDirSuccess, fail);
    }

    function getTruvleDirSuccess(dirEntry) {
    
         dirEntry.getFile("testtheme2.min.css", {create: true}, getCSSFileSucces, fail); 
         
    }

    function getCSSFileSucces(fileEntry) {
        fileEntry.createWriter(getCSSFileWriter, fail);
    }

    function getCSSFileWriter(writer) {


       
        writer.onwrite = function (evt) {
            console.log("write success");
        };

        var CSSserviceURL = getServiceURL() + "GetCSS?id=0";


        $.getJSON(CSSserviceURL, function(data) {
		    writer.write(data);
	    });	
        
         if (document.createStyleSheet){
             document.createStyleSheet(CSSLocalPath);
            }
            else {
                $("head").append($("<link rel='stylesheet' href='" + CSSLocalPath + "' type='text/css' media='screen' />"));
            }

    }
    
    function fail(error) {
        alert(error.code);    
    }
    

//UpdateCSS functions


//Inbox function

    function ShowAll()
    {
        $("#tripitemlist li").each(function(){
               $(this).show();
        });
        $("#tripitemlist").listview('refresh');
    }
    function ShowFlights()
    {
        $("#tripitemlist li").each(function(){
               $(this).show();
        });
        var SEARCHWORD = "0";
        $("#tripitemlist li").each(function(){
            if($(this).attr("data-filtertext").toUpperCase().indexOf(SEARCHWORD.toUpperCase()) >=0)
               $(this).show();
            else
               $(this).hide();
        });
        $('#tripitemlist').listview('refresh');
    }
    function ShowSO()
    {
        $("#tripitemlist li").each(function(){
               $(this).show();
        });
        var SEARCHWORD = "1";
        $("#tripitemlist li").each(function(){
            if($(this).attr("data-filtertext").toUpperCase().indexOf(SEARCHWORD.toUpperCase()) >=0)
               $(this).show();
            else
               $(this).hide();
        });
        $('#tripitemlist').listview('refresh');
    }
    function ShowOther()
    {
        var SEARCHWORD = "2";
        $("#tripitemlist li").each(function(){
            if($(this).attr("data-filtertext").toUpperCase().indexOf(SEARCHWORD.toUpperCase()) >=0)
               $(this).show();
            else
               $(this).hide();
        });
        $('#tripitemlist').listview('refresh');
    }
//End of Inbox function


function getServiceURL() {

    if (sessionStorage.getItem("fromOffice") != null) {
        if (sessionStorage.getItem("fromOffice"))
            return webServiceLocationDev;
        else
            return webServiceLocationProd;
    }
    else {
        return webServiceLocationProd;
    }
}

           
function Logout()
{
     sessionStorage.removeItem("userName");            
     sessionStorage.removeItem("userID");            
     OpenPage('loginpagePage');  
}
        
        
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


function setbuttonpressed(myButton) {
    myButton.src = myButton.src.replace('btn', 'btnPressed');
}


    