﻿
var AstricomTravelDB;

function GetDB()
{    
    return AstricomTravelDB;
}   



    // Populate the database 
    //
function createDB(tx) {
       
    //tx.executeSql('DROP TABLE IF EXISTS LOCATIONS');
    tx.executeSql('CREATE TABLE IF NOT EXISTS LOCATIONS' +
                              '(id INTEGER PRIMARY KEY, title TEXT, description TEXT, imagePath TEXT, lat REAL, long REAL, address TEXT, tripID INTEGER)');
	
	
	//tx.executeSql('DROP TABLE IF EXISTS TRIPEVENT');
	tx.executeSql('CREATE TABLE IF NOT EXISTS TRIPEVENT (' + 
        'EventID INTEGER PRIMARY KEY ASC  ,' + 
        'EventSetID INTEGER,' + 
        'EventType TEXT,' + 
        'EventLat TEXT,' + 
        'EventLng TEXT,' + 
        'Name TEXT,' + 
        'StartDate DATE,' + 
        'EndDate DATE,' + 
        'DataJSon TEXT,' + 
        'Description TEXT,' + 
        'Place TEXT,' + 
        'Address TEXT,' + 
        'Phone TEXT,' + 
        'Email TEXT,' + 
        'Contact TEXT)'); 
}
            

function GetItirenaryList()
{

    if(reloadItinerary)
    {
        reloadItinerary = false;
        console.log('GetItirenaryList');
        var db = GetDB();
        db.transaction(GetItirenaryListExecSQL);
        ShowLoading();
    }
}
function GetItirenaryListExecSQL(tx)
{
    console.log('GetItirenaryListExecSQL');
    tx.executeSql('SELECT * FROM TRIPEVENT order by StartDate asc', [], GetItirenaryListQuerySuccess, errorCB);
}

function GetItirenaryListQuerySuccess(tx, results) {
    console.log('GetItirenaryListQuerySuccess');


    if(results.rows.length == null || results.rows.length == 0)
        $("#itirenarylistmessage").html(noitirenaryresult);
    
    var len = results.rows.length;
    //console.log("Results : " + JSON.stringify(results.rows.item));
    
    var LastDateDivider;
    
    $('#itirenarylist li').remove();
    
    for (var i=0; i<len; i++){
        var myItem = results.rows.item(i);
        
        var currStartDate = new Date();
        currStartDate.setTime(results.rows.item(i).StartDate);
        var currDate = currStartDate.toDateString();
        var currTime = currStartDate.toLocaleTimeString().replace(new RegExp(":.." + '$'), '');
        
        if(currStartDate.toDateString() != LastDateDivider)
        {
            LastDateDivider = currStartDate.toDateString();
            $( "#itinerarydivideritem" ).tmpl(  { "Date" : currDate } )
    		            .appendTo( "#itirenarylist" );
        }            
    
        if(myItem.DataJSon != "")
        {
            console.log('myJson: ' + myItem.DataJSon);
            var myObj = jQuery.parseJSON(myItem.DataJSon);
            if(myItem.EventType == "flight")
            {                
                $( "#itirenaryflightitem" ).tmpl(  { "EventID" : myItem.EventID, "filtertype" : "flight",
                                    "EventType" : myItem.EventType, 
                                   "ImageURL" : GetImageByEventType(myItem.EventType), "Time" : currTime, "FlightObject" :  myObj} )
		            .appendTo( "#itirenarylist" );
            }
            else
            {
                $( "#itirenarygoogleplacesitem" ).tmpl(  { "EventID" : myItem.EventID, "filtertype" : GetFilterByEventType(myItem.EventType), 
                                     "EventType" : myItem.EventType, 
                                    "ImageURL" : GetImageByEventType(myItem.EventType), "Time" : currTime, "GooglePlaceObject" :  myObj } )
		            .appendTo( "#itirenarylist" );
            }
        }
        else
        {
             $( "#itirenaryuserdefineditem" ).tmpl(  { "EventID" : myItem.EventID,  "filtertype" : GetFilterByEventType(myItem.EventType), 
                                    "EventType" : myItem.EventType,
                                    "ImageURL" : GetImageByEventType(myItem.EventType), "Name" : myItem.Name, "Description" : myItem.Description,
                                     "Time" : currTime, "Place" : myItem.Place, "Address" : myItem.Address, "Phone" : myItem.Phone } )
		            .appendTo( "#itirenarylist" );
        }   
        $('#itirenarylist').listview('refresh'); 		            
        //console.log("Row = " + i + " ID = " + myItem.EventID + " Type =  " + myItem.EventType + " Name =  " + myItem.Name);
        //console.log( $('#itirenarylist').html());
        
    }
    HideLoading();
}

function GetItirenaryItem()
{
    console.log('GetItirenaryItem');
    var db = GetDB();
    db.transaction(GetItirenaryItemExecSQL);
}

function GetItirenaryItemExecSQL(tx)
{
    
    if(sessionStorage.ItirenaryDetailsEventID != null)
    {
        console.log('GetItirenaryItemExecSQL');
        tx.executeSql('SELECT * FROM TRIPEVENT where EventID=' + sessionStorage.ItirenaryDetailsEventID,
                     [], GetItirenaryItemQuerySuccess, errorCB);
    }
    else
    {
        alert("There was a problem loading item details.");
    }
}

function GetItirenaryItemQuerySuccess(tx, results) {
    console.log('GetItirenaryItemQuerySuccess');


    if(results.rows.length == null || results.rows.length == 0)
        alert("There was a problem loading item details.");
    
    var len = results.rows.length;
    console.log("Events table: " + len + " rows found.");
    

    for (var i=0; i<len; i++){
        var myItem = results.rows.item(i);
        
        console.log("Row = " + i + " ID = " + myItem.EventID + " Type =  " + myItem.EventType + " Name =  " + myItem.Name + " Json =  " + myItem.DataJSon);
        var currStartDate = new Date();
        currStartDate.setTime(results.rows.item(i).StartDate);
        var currDate = currStartDate.toDateString();
        var currTime = currStartDate.toLocaleTimeString().replace(new RegExp(":.." + '$'), '');
    
        if(myItem.DataJSon != "")
        {
            var myObj = jQuery.parseJSON(myItem.DataJSon);
            if(myItem.EventType == "flight")
            {                                
                PopulateFlightDetails(myItem.DataJSon);
            }
            //Google Place Item
            else
            {
                var myLoc= new google.maps.LatLng(myItem.EventLat, myItem.EventLng);
                myObj.geometry.location = myLoc;

                populateGooglePlaceDetails(myObj, google.maps.places.PlacesServiceStatus.OK);
            }
        }
        else
        {
            populateUserDefinedDetails(currTime, myItem);
        }      
    }
    HideLoading();
}

function DeleteItemFromItinerary()
{
    console.log('DeleteItirenaryItem');
    var db = GetDB();
    db.transaction(DeleteItirenaryItemExecSQL);
}

function DeleteItirenaryItemExecSQL(tx)
{
    
    if(sessionStorage.ItirenaryDetailsEventID != null)
    {
        console.log('DeleteItirenaryItemExecSQL');
        tx.executeSql('DELETE FROM TRIPEVENT where EventID=' + sessionStorage.ItirenaryDetailsEventID,
                     [], DeleteItirenaryItemQuerySuccess, errorCB);
    }
    else
    {
        alert("There was a problem deleting item.");
    }
}

function DeleteItirenaryItemQuerySuccess(tx, results) {
    console.log('DeleteItirenaryItemSuccess');
    reloadItinerary = true;		
	DeleteItirenaryItemInSession();
	
	OpenPage('itineraryPage');
}



function InsertItirenaryItem() {
    console.log('InsertItirenaryItem');
	var db = GetDB();
	db.transaction(GetEventID);
    db.transaction(InsertItirenaryItemExecSQL, errorCB, InsertItirenaryItemSuccess);
}

function GetEventID(tx) {
    console.log('GetEventID');
	var select = "SELECT MAX(EventID) as 'maxeventid' FROM TRIPEVENT";
	console.log(select);
	tx.executeSql(select, [], GetEventIDSuccess, errorCB);
}

function GetEventIDSuccess(tx, results) {
    console.log('GetEventIDSuccess');
    var EventID
    if(results.rows.item(0).maxeventid != null)
    {
	    console.log('maxeventid: ' + results.rows.item(0).maxeventid);
	    EventID = results.rows.item(0).maxeventid + 1;
	}
	else
	    EventID = 0;
	sessionStorage.setItem("EventID", EventID);
}

function InsertItirenaryItemExecSQL(tx)
{

    if (sessionStorage.getItem("EventID")!= null) {
    
        EventID = replaceAll(sessionStorage.EventID, '\'', '');
        EventSetID = replaceAll(sessionStorage.EventSetID, '\'', '');
        EventType = replaceAll(sessionStorage.EventType, '\'', '');
        EventLat = replaceAll(sessionStorage.EventLat, '\'', '');
        EventLng = replaceAll(sessionStorage.EventLng, '\'', '');
        Name = replaceAll(sessionStorage.Name, '\'', '');
        StartDate = replaceAll(sessionStorage.StartDate, '\'', '');
        EndDate = replaceAll(sessionStorage.EndDate, '\'', '');
        DataJSon = replaceAll(sessionStorage.DataJSon, '\'', '');
        Description = replaceAll(sessionStorage.Description, '\'', '');
        Place = replaceAll(sessionStorage.Place, '\'', '');
        Address = replaceAll(sessionStorage.Address, '\'', '');
        Phone = replaceAll(sessionStorage.Phone, '\'', '');
        Email = replaceAll(sessionStorage.Email, '\'', '');
        Contact = replaceAll(sessionStorage.Contact, '\'', '');

    }

    var insert = 'INSERT INTO TRIPEVENT (EventID, EventSetID, EventType, EventLat, EventLng, Name, ' +
                  'StartDate, EndDate, DataJSon, Description, Place, Address, Phone, Email, Contact)' +
                  ' VALUES (' +  EventID + ', ' + EventSetID + ', \'' + EventType + '\', \'' + EventLat + '\', \'' + EventLng + '\', \'' 
                  + Name +  '\', ' + StartDate + ', ' + EndDate + ', \'' + DataJSon + '\', \'' + Description + 
                  '\', \'' + Place + '\', \'' + Address + '\', \'' + Phone + '\', \'' + Email + '\', \'' + Contact+ '\')';
                  
    console.log(insert);
    tx.executeSql(insert);
}

function replaceAll(txt, replace, with_this) {
  if(txt != null && txt != "" && txt != 'undefined')
    return txt.replace(new RegExp(replace, 'g'),with_this);
 else
    return "";
}

function InsertItirenaryItemSuccess() {
    console.log('InsertItirenaryItemSuccess');
    reloadItinerary = true;		
	DeleteItirenaryItemInSession();
	
	OpenPage('itineraryPage');
}


function successCB() {
    console.log("success!");
}      

function errorCB(err) {
    console.log("Error processing SQL: "+err.code);
}
