function findmycarPageShow(){
                         //SaveCurrNavPos();
                         var db = GetDB();
                         db.transaction(fetchDB);                         
}

function fetchDB(tx) {
	var select = "SELECT * FROM LOCATIONS";
	tx.executeSql(select, [], successFetch, errorCB);
}

function successFetch(tx, results) {
    
    $('#locations').children().remove('li');
	var len = results.rows.length;
	var pos;
	//sessionStorage.setItem("number", len);
    for (var i=0; i<len; i++){
    	pos = results.rows.item(i);
        console.log("Row = " + i + " ID = " + pos.id + " title =  " + pos.title + " Description: " + pos.description + " Path: " + pos.address);
        $('#locations').append('<li><a href="" onClick="rememberPosition(\'' 
                               + pos.title + '\', \'' 
                               + pos.description + '\', \''
                               + pos.id + '\', \'' 
                               + pos.imagePath + '\', \'' 
                               + pos.lat + '\', \''
                               + pos.long + '\',true)" >'
                               +
                               '<h4>' + pos.title + '</h4>' +
                               '<p>' + pos.description +  '</p>' +
                               '</a></li>');
		$('#locations').listview('refresh');
    }
}


function updateCountdown() {
    var max = 150;
    var remaining = max - jQuery('.message').val().length;
    jQuery('.countdown').text(remaining + '/' + max);
}


function rememberPosition(title, description, id, imagePath, lat, long, andShow) {
	sessionStorage.setItem("id", id);	
	sessionStorage.setItem("title", title);	
	sessionStorage.setItem("description", description);	
	sessionStorage.setItem("imagePath", imagePath);	
	sessionStorage.setItem("locLat", lat);
	sessionStorage.setItem("locLong", long);
    if (andShow)
    {
        showPopup();
    }
}
