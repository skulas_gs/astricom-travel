﻿// Search Message Strings
var typetostart = "3 letters min.";
var nocityresult = "No city matches the criteria.";
var noairportresult = "No airport matches the criteria.";
var noflightresult = "No flight matches the criteria.";
var noitirenaryresult = "No items in itirenary.";

//
// Astricom Server (QA)
//
var webServiceLocationProd = "http://gallopstars.co.il:83/Search.svc/";
var webServiceLocationDev = "http://10.0.0.69:83/Search.svc/";


//
// free.worldweatheronline.com
//

//City Search Autocomplete
function GetweatherserviceURL(currLat, currLong)
{
    return "http://free.worldweatheronline.com/feed/weather.ashx?q=" +
            currLat +
            "," +
            currLong +
            "&format=json&num_of_days=5&key=294ceedd48135136120905";
}


//
// gd.geobytes.com
//
//http://www.geobytes.com/free-ajax-cities-jsonp-api.htm
//
//
//Do I need an API Key? 

//No, but if you expect to be performing more than 50,000 
//requests per day (your average unique visitors X 5), 
//then please tell us. 
//(You could even consider buying $10 worth of Mapbytes 
//so that you have a current account in our system - 
//but you certainly don't have to.)

//City Search Autocomplete
function GetGeoBytesAutoCompleteCity(searchstring)
{
    return "http://gd.geobytes.com/AutoCompleteCity?callback=?&q=" + searchstring;
}

//City Details
function GetGeoBytesCityDetails(searchstring)
{
    return "http://gd.geobytes.com/GetCityDetails?callback=?&fqcn=" + searchstring;
}



//
// flightstats.com
//

//Airport Search
function GetAirportService(searchstring)
{
    return "http://www.pathfinder-xml.com/development/json?" +
            "Service=AirportGetAirportsService" +
            "&login.accountID=8424&login.userID=smitenka&login.password=shigaon" +
            "&airportGetAirportsInfo.specificationMatching.matchString=" + searchstring;
}
    
//FlightSearch
function GetFlightsService(startdate, enddate, depCode, ariCode)
{
     return "https://www.pathfinder-xml.com/development/json?Service=FlightHistoryGetRecordsService" + 
            "&info.flightHistoryGetRecordsRequestedData.codeshares=true&login.guid=&login.accountID=8424&login.userID=smitenka&login.password=shigaon" + 
            "&info.specificationDateRange.departureDateTimeMin=" + startdate + "T00%3A00" +
            "&info.specificationDateRange.departureDateTimeMax=" + enddate + "T00%3A00" + 
            "&info.specificationDateRange.arrivalDateTimeMin=" +
            "&info.specificationDateRange.arrivalDateTimeMax=" + 
            "&info.specificationFlights[0].airline.airlineCode=&info.specificationDepartures[0].airport.airportCode=" + depCode + 
            "&info.specificationFlights[0].flightNumber=&info.specificationArrivals[0].airport.airportCode=" + ariCode + 
            "&info.specificationFlights[0].searchCodeshares=&info.flightHistoryGetRecordsRequestedData.codeshares=true&Submit=Submit";
} 
   
function ShowLoading()
{ 
    $.mobile.showPageLoadingMsg("b", "Loading Data...", true);
}
function HideLoading()
{ 
    $.mobile.hidePageLoadingMsg();
}