﻿
function guidesareaselectPageShow(){
    $("#guideslistmessage").html(typetostart);
    $("#searchField").on("input", GetCityList);
    
    $('.ui-input-clear').live('click', function(e){
      $('#guidessuggestions li').remove();
      $('#guidessuggestions').listview("refresh");        
      $("#guideslistmessage").html(typetostart);
    });
}


function CheckEmptyCityList(){
    //if($("#guidessuggestions li").length == 0 || $("#guidessuggestions li").length == hiddencount)    
    if($("#guidessuggestions li").length == 0)
        $("#guideslistmessage").html(nocityresult);
    else
        $("#guideslistmessage").html("");

}      
   
function GetCityList(e) {
    
    var sugList = $("#guidessuggestions");  

    var text = $(this).val();
    if(text.length < 3)
    {
        $("#guideslistmessage").html(typetostart);
        $('#guidessuggestions li').remove();
    }
    else
    {		
//        var hiddencount = 0;    		                    
//        if(text.length == 3 && $("#guidessuggestions li").length == 0 )
//        {   
            $('#guidessuggestions li').remove();
            $.get( GetGeoBytesAutoCompleteCity(text), function(res,code) {                
                var k = 0;
                for(var i=0, len=res.length; i<len; i++) {
                    if(res[i] != "")
                    {
                        sugList.append('<li data-filtertext="' + res[i] + '">' +
                            '<a href="#" onclick="getcitydetails(\''+res[i]+'\');">'+res[i]+'</a></li>');
                    }
                }
                sugList.listview("refresh");  
                CheckEmptyCityList();
            } 
            ,"jsonp");  
//        }
//        else
//        {			    
//            $("#guidessuggestions li").each(function(){
//                if($(this).attr("data-filtertext").toUpperCase().indexOf(text.toUpperCase()) >=0)
//                   $(this).show();
//                else
//                {
//                   hiddencount++;
//                   $(this).hide();
//                }
//            });
//            sugList.listview("refresh");  
//            CheckEmptyList();
//        } 
    }     
}


function getcitydetails(fqcn) {

	if (typeof fqcn == "undefined") 
	    fqcn = $("#searchField").val();

	cityfqcn = fqcn;

	if (cityfqcn) {
	 jQuery.getJSON(
			GetGeoBytesCityDetails(cityfqcn),
		 function (data) {
		     saveCurrPos(data.geobyteslatitude, data.geobyteslongitude);
		     sessionStorage.searchnearcity= fqcn;
		     OpenPage('guidesmainmenuPage');  
		 }
	 );
	}
}